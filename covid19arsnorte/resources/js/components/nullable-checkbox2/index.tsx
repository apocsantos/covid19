import React from "react";
import { Checkbox } from "@material-ui/core";

interface NullableCheckbox2Props {
    checked: boolean | null;
    onChange: (c: boolean | null) => void;
}

export function NullableCheckbox2({
    checked,
    onChange
}: NullableCheckbox2Props): JSX.Element {
    // const [_checked, setChecked] = useState(false);
    const handleChange = (event): void => {
        onChange(!!event.target.checked);
    };

    return (
        <Checkbox
            checked={!!checked}
            indeterminate={checked === null}
            onChange={handleChange}
        ></Checkbox>
    );
}
