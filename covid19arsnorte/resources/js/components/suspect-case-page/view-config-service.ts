import { SYMPTOM_TYPE, SymptomMap } from "./types";

function getClinicalCaseConfig(): Promise<SymptomMap> {
    const config: SymptomMap = {
        fever: {
            id: "fever",
            type: SYMPTOM_TYPE.CHECK,
            name: "Febre",
            value: false,
            group: 0
        },
        cough: {
            id: "cough",
            type: SYMPTOM_TYPE.CHECK,
            name: "Tosse",
            value: false,
            group: 0
        },
        chills: {
            id: "chills",
            type: SYMPTOM_TYPE.CHECK,
            name: "Calafrios",
            value: false,
            group: 0
        },
        otophagia: {
            id: "otophagia",
            type: SYMPTOM_TYPE.CHECK,
            name: "Odinofagia",
            value: false,
            group: 0
        },
        coryza: {
            id: "coryza",
            type: SYMPTOM_TYPE.CHECK,
            name: "Coriza",
            value: false,
            group: 0
        },
        headaches: {
            id: "headaches",
            type: SYMPTOM_TYPE.CHECK,
            name: "Cefaleia",
            value: false,
            group: 0
        },
        myalgia: {
            id: "myalgia",
            type: SYMPTOM_TYPE.CHECK,
            name: "Mialgia",
            value: false,
            group: 0
        },
        dyspnoea: {
            id: "dyspnoea",
            type: SYMPTOM_TYPE.CHECK,
            name: "Dispneia",
            value: false,
            group: 0
        },
        abdominalPain: {
            id: "abdominalPain",
            type: SYMPTOM_TYPE.CHECK,
            name: "Dor abdominal",
            value: false,
            group: 0
        },
        vomits: {
            id: "vomits",
            type: SYMPTOM_TYPE.CHECK,
            name: "Vomitos",
            value: false,
            group: 0
        },
        diarrhea: {
            id: "diarrhea",
            type: SYMPTOM_TYPE.CHECK,
            name: "Diarreia",
            value: false,
            group: 0
        },
        otherSymptoms: {
            id: "otherSymptoms",
            type: SYMPTOM_TYPE.FREE_TEXT,
            name: "Outros",
            value: "",
            group: 1
        }
    };

    return Promise.resolve(config);
}

export { getClinicalCaseConfig };
