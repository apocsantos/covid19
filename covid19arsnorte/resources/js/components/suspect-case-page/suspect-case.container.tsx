import React, { useState, useEffect } from "react";
import {
    Button,
    Snackbar,
    FormControlLabel,
    Card,
    CardContent,
    Grid
} from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";

import { NullableCheckbox } from "../nullable-checkbox";
import { SearchCase } from "../search-case";
import { ClinicalPicture } from "./clinical-picture";
import { TravelPicture } from "./travel-picture";

import { getClinicalCaseConfig } from "./view-config-service";
import { useSuspect } from "./services";
import { Symptom, SymptomMap, Suspect } from "./types";
import { IdentificationPicture } from "./identification-picture";
import { DOCUMENT_TYPES } from "../../types";
import { NullableCheckbox2 } from "../nullable-checkbox2";

function Alert(props: AlertProps): JSX.Element {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SuspectedCaseContainer(): JSX.Element {
    const [isPositive, setIsPositive] = useState(null);
    const [documentType, setDocumentType] = useState(DOCUMENT_TYPES.SNS);
    const [documentNumber, setDocumentNumber] = useState("");
    const [name, setName] = useState("");
    const [birthDate, setBirthDate] = useState(null);
    const [gender, setGender] = useState("");
    const [nationality, setNationality] = useState("");
    const [profession, setProfession] = useState("");
    const [email, setEmail] = useState("");
    const [residence, setResidence] = useState("");
    const [outsideCountryResidence, setOutsideCountryResidence] = useState("");
    const [location, setLocation] = useState("");
    const [contact, setContact] = useState("");
    const [friendContact, setFriendContact] = useState("");
    const [registration, setRegistration] = useState("");
    const [symptomsMap, setSymptoms] = useState<SymptomMap>({});
    const [symptomDate, setSymptomDate] = useState<Date>(new Date());
    const [traveledToForeign, setTraveledToForeign] = useState<boolean | null>(
        null
    );
    const [foreignCountry, setForeignCountry] = useState("");
    const [startTravelDate, setStartTravelDate] = useState(null);
    const [returnTravelDate, setReturnTravelDate] = useState(null);
    const [returnToCountryDate, setReturnToCountryDate] = useState(null);
    const [contactWithInfected, setContactWithInfected] = useState("");
    const [confirmedCaseName, setConfirmedCaseName] = useState("");
    const [confirmedCaseContactDate, setConfirmedCaseContactDate] = useState(
        null
    );
    const [isApproved, setIsApproved] = useState(false);
    const [isHealthProfessional, setIsHealthProfessional] = useState(null);
    const [
        suspect,
        search,
        updateSuspect,
        clearSuspect,
        clearStateHandlers,
        isLoaded,
        searchError,
        isSaved,
        saveError
    ] = useSuspect();

    const clear = (): void => {
        clearSuspect();
        setDocumentType(DOCUMENT_TYPES.SNS);
        setName("");
        setBirthDate(null);
        setGender("");
        setNationality("");
        setProfession("");
        setEmail("");
        setResidence("");
        setOutsideCountryResidence("");
        setLocation("");
        setContact("");
        setFriendContact("");
        setRegistration("");
        setDocumentNumber("");
        setForeignCountry("");
        setSymptomDate(new Date());
        setTraveledToForeign(null);
        setStartTravelDate(null);
        setReturnTravelDate(null);
        setReturnToCountryDate(null);
        setContactWithInfected("");
        setConfirmedCaseName("");
        setConfirmedCaseContactDate(null);
        setIsHealthProfessional(null);
        setIsApproved(false);
        setSymptoms(
            Object.keys(symptomsMap).reduce(
                (acc, curr) => ({
                    ...acc,
                    [curr]: {
                        ...symptomsMap[curr],
                        value: curr == "otherSymptoms" ? "" : false
                    }
                }),
                {}
            )
        );
    };

    useEffect(() => {
        async function fetchClinicalPictureConfig(): Promise<void> {
            const config = await getClinicalCaseConfig();

            setSymptoms(config);
        }

        fetchClinicalPictureConfig();
    }, []);

    useEffect(() => {
        if (!suspect) {
            clear();
            return;
        }

        const newSymptomsMap = Object.fromEntries(
            Object.entries(symptomsMap).map(([key, symptom]) => {
                return [key, { ...symptom, value: suspect[symptom.id] }];
            })
        );

        setIsPositive(suspect.is_positive);
        setName(suspect.name ?? "");
        setBirthDate(suspect.birth_date ?? "");
        setGender((suspect.gender ?? "").toLowerCase());
        setNationality(suspect.nationality ?? "");
        setProfession(suspect.profession ?? "");
        setEmail(suspect.email ?? "");
        setResidence(suspect.address ?? "");
        setOutsideCountryResidence(suspect.outside_country_residence ?? "");
        setLocation(suspect.location ?? "");
        setContact(suspect.contact ?? "");
        setFriendContact(suspect.friend_contact ?? "");
        setRegistration(suspect.registration ?? "");
        setDocumentNumber(suspect.id_number ?? "");
        setDocumentType(suspect.id_type);
        setSymptomDate(suspect.date_first_symptoms ?? new Date());
        setForeignCountry(suspect.recent_trips);
        setTraveledToForeign(
            !!(
                suspect.trip_departure ||
                suspect.trip_return ||
                suspect.recent_trips
            )
        );
        setStartTravelDate(suspect.trip_departure ?? null);
        setReturnTravelDate(suspect.trip_return ?? null);
        setReturnToCountryDate(suspect.trip_return_portugal ?? null);
        setContactWithInfected(suspect.contact_confirmed_case);
        setConfirmedCaseName(suspect.contact_confirmed_case_name ?? "");
        setConfirmedCaseContactDate(
            suspect.contact_confirmed_case_date ?? null
        );
        setIsHealthProfessional(suspect.is_health_profissional ?? false);
        setIsApproved(suspect.is_approved);
        setSymptoms(newSymptomsMap);
    }, [suspect]);

    const saveData = async (event): Promise<void> => {
        event.preventDefault();

        const symptoms = Object.fromEntries(
            Object.entries(symptomsMap).map(([key, symptom]) => [
                key,
                symptom.value
            ])
        ) as Partial<Suspect>;

        const entry: Suspect = {
            id: suspect?.id,
            id_number: documentNumber,
            id_type: documentType,
            name: name,
            birth_date: birthDate,
            gender: gender,
            contact: contact,
            address: residence,
            outside_country_residence: outsideCountryResidence,
            registration: registration,
            friend_contact: friendContact,
            date_first_symptoms: symptomDate,
            ...symptoms,
            location: location,
            recent_trips: foreignCountry,
            trip_departure: startTravelDate,
            trip_return: returnTravelDate,
            trip_return_portugal: returnToCountryDate,
            contact_confirmed_case: contactWithInfected,
            contact_confirmed_case_name: confirmedCaseName,
            contact_confirmed_case_date: confirmedCaseContactDate,
            is_health_profissional: isHealthProfessional,
            is_approved: isApproved,
            nationality,
            profession,
            email,
            originalEntry: suspect?.originalEntry
        } as Suspect;

        updateSuspect(entry);
        clear();
    };

    const symptomsByGroup = Object.values(symptomsMap).reduce<{
        [id: string]: Symptom[];
    }>(
        (acc, curr) => ({
            ...acc,
            [curr.group]: [...(acc[curr.group] ?? []), curr]
        }),
        {}
    );

    const handleDocumentTypeChange = (documentType: DOCUMENT_TYPES): void =>
        setDocumentType(documentType);

    const handleDocumentNumberChange = (documentNumber: string): void =>
        setDocumentNumber(documentNumber);

    const handleNameChange = (name: string): void => setName(name);

    const handleBirthDateChange = (date: Date): void => setBirthDate(date);

    const handleGenderChange = (gender: string): void => setGender(gender);

    const handleNationalityChange = (nationality: string): void =>
        setNationality(nationality);

    const handleProfessionChange = (profession: string): void =>
        setProfession(profession);

    const handleEmailChange = (email: string): void => setEmail(email);

    const handleResidenceChange = (res: string): void => setResidence(res);

    const handleOutsideCountryResidenceChange = (countryOfRes: string): void =>
        setOutsideCountryResidence(countryOfRes);

    const handleLocationChange = (location: string): void =>
        setLocation(location);

    const handleContactChange = (contact: string): void => setContact(contact);

    const handleFriendContactChange = (friendContact: string): void =>
        setFriendContact(friendContact);

    const handleRegistrationChange = (registration: string): void =>
        setRegistration(registration);

    const symptoms = Object.values(symptomsByGroup);

    const handleSymptomChange = (symptom: Symptom): void => {
        const newSymptoms = {
            ...symptomsMap,
            [symptom.id]: symptom
        };

        setSymptoms(newSymptoms);
    };
    const handleDateChange = (date: Date): void => setSymptomDate(date);
    const handleTraveledToForeignChange = (hasTraveled: boolean): void =>
        setTraveledToForeign(hasTraveled);

    const handleForeignCountryChange = (country: string): void =>
        setForeignCountry(country);

    const handleStartTravelDateChange = (date: Date): void =>
        setStartTravelDate(date);

    const handleReturnTravelDateChange = (date: Date): void =>
        setReturnTravelDate(date);

    const handleContactWithInfectedChange = (
        contactWithInfected: string
    ): void => setContactWithInfected(contactWithInfected);

    const handleConfirmedCaseNameChange = (confirmedCaseName: string): void =>
        setConfirmedCaseName(confirmedCaseName);

    const handleConfirmedCaseContactDateChange = (date: Date): void =>
        setConfirmedCaseContactDate(date);

    const handleIsHealthProfessionalChange = (
        isHealthProfessional: boolean | null
    ): void => setIsHealthProfessional(isHealthProfessional);

    const handleSearchClicked = async (idType, idNumber): Promise<void> => {
        await search(idType, idNumber);
    };

    const handleIsApprovedChange = (newValue: boolean): void => {
        setIsApproved(newValue);
    };

    const handleErrorClose = (): void => {
        if (!!searchError) {
            clearSuspect();
        }
    };

    const handleSuccessClose = (): void => {
        clearStateHandlers();
    };

    return (
        <Grid container direction="row" spacing={3}>
            <Grid item xs={12}>
                <Card>
                    <CardContent>
                        <SearchCase
                            onSearchClicked={handleSearchClicked}
                            onClear={clear}
                        />
                    </CardContent>
                </Card>
            </Grid>
            <Snackbar
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
                onClose={handleErrorClose}
                open={!!searchError || !!saveError}
                autoHideDuration={5000}
            >
                <Alert severity="error" onClose={handleErrorClose}>
                    {searchError}
                </Alert>
            </Snackbar>
            <Snackbar
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
                onClose={handleSuccessClose}
                open={!!isSaved || !!isLoaded}
                autoHideDuration={5000}
            >
                <Alert severity="success" onClose={handleSuccessClose}>
                    Operação executada com Sucesso!
                </Alert>
            </Snackbar>
            {suspect && (
                <NullableCheckbox label="Positivo" value={isPositive} />
            )}
            <Grid item>
                <form autoComplete="off" onSubmit={saveData}>
                    <Grid container direction="row" spacing={1}>
                        <Grid item xs={12}>
                            <Card>
                                <CardContent>
                                    <IdentificationPicture
                                        name={name}
                                        birthDate={birthDate}
                                        gender={gender}
                                        nationality={nationality}
                                        profession={profession}
                                        email={email}
                                        residence={residence}
                                        outsideCountryResidence={
                                            outsideCountryResidence
                                        }
                                        location={location}
                                        contact={contact}
                                        friendContact={friendContact}
                                        registration={registration}
                                        documentNumber={documentNumber}
                                        documentType={documentType}
                                        onNameChange={handleNameChange}
                                        onBirthDateChange={
                                            handleBirthDateChange
                                        }
                                        onGenderChange={handleGenderChange}
                                        onNationalityChange={
                                            handleNationalityChange
                                        }
                                        onProfessionChange={
                                            handleProfessionChange
                                        }
                                        onEmailChange={handleEmailChange}
                                        onResidenceChange={
                                            handleResidenceChange
                                        }
                                        onOutsideCountryResidenceChange={
                                            handleOutsideCountryResidenceChange
                                        }
                                        onLocationChange={handleLocationChange}
                                        onContactChange={handleContactChange}
                                        onFriendContactChange={
                                            handleFriendContactChange
                                        }
                                        onRegistrationChange={
                                            handleRegistrationChange
                                        }
                                        onDocumentTypeChange={
                                            handleDocumentTypeChange
                                        }
                                        onDocumentNumberChange={
                                            handleDocumentNumberChange
                                        }
                                    />
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12}>
                            <Card>
                                <CardContent>
                                    <ClinicalPicture
                                        symptomGroups={symptoms}
                                        onSymptomChange={handleSymptomChange}
                                        onSymptomDateChange={handleDateChange}
                                    />
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12}>
                            <Card>
                                <CardContent>
                                    <TravelPicture
                                        traveledToForeign={traveledToForeign}
                                        foreignCountry={foreignCountry}
                                        startTravelDate={startTravelDate}
                                        returnTravelDate={returnTravelDate}
                                        contactWithInfected={
                                            contactWithInfected
                                        }
                                        confirmedCaseName={confirmedCaseName}
                                        confirmedCaseContactDate={
                                            confirmedCaseContactDate
                                        }
                                        isHealthProfessional={
                                            isHealthProfessional
                                        }
                                        onTraveledToForeignChange={
                                            handleTraveledToForeignChange
                                        }
                                        onStartTravelDateChange={
                                            handleStartTravelDateChange
                                        }
                                        onReturnTravelDateChange={
                                            handleReturnTravelDateChange
                                        }
                                        onForeignCountryChange={
                                            handleForeignCountryChange
                                        }
                                        onConfirmedCaseContactDateChange={
                                            handleConfirmedCaseContactDateChange
                                        }
                                        onContactWithInfectedChange={
                                            handleContactWithInfectedChange
                                        }
                                        onConfirmedCaseNameChange={
                                            handleConfirmedCaseNameChange
                                        }
                                        onIsHealthProfessionalChange={
                                            handleIsHealthProfessionalChange
                                        }
                                    />
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>

                    <FormControlLabel
                        control={
                            <NullableCheckbox2
                                checked={isApproved}
                                onChange={handleIsApprovedChange}
                            ></NullableCheckbox2>
                        }
                        label={"Aprovado?"}
                    />

                    <Button variant="contained" color="primary" type="submit">
                        Save
                    </Button>
                </form>
            </Grid>
        </Grid>
    );
}

export { SuspectedCaseContainer };
