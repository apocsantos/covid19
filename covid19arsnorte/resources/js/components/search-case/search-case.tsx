import React, { useState } from "react";
import {
    InputLabel,
    Select,
    MenuItem,
    FormControl,
    TextField,
    Button,
    Grid
} from "@material-ui/core";
import { DOCUMENT_TYPES_LABELS } from "../../constants";
import { DOCUMENT_TYPES } from "../../types";

interface SearchCaseProps {
    onSearchClicked: (idType: string, idNumber: string) => void;
    onClear: () => void;
}

function SearchCase({
    onSearchClicked,
    onClear
}: SearchCaseProps): JSX.Element {
    const [idNumber, setIdNumber] = useState(null);
    const [idType, setIdType] = useState(DOCUMENT_TYPES.SNS);

    const handleClearClick = (): void => {
        setIdNumber(null);
        setIdType(DOCUMENT_TYPES.SNS);
        onClear();
    };

    return (
        <form
            onSubmit={event => {
                event.preventDefault();
                onSearchClicked(idType, idNumber);
            }}
            autoComplete="off"
        >
            <Grid
                container
                direction="row"
                justify="space-between"
                wrap="wrap"
                style={{ marginBottom: 10 }}
            >
                <Grid item>
                    <FormControl style={{ minWidth: 140, marginRight: 10 }}>
                        <InputLabel id="id_type">Tipo Documento</InputLabel>
                        <Select
                            value={idType || ""}
                            labelId="id_type"
                            id="id_type_selector"
                            onChange={event =>
                                setIdType(event.target.value as DOCUMENT_TYPES)
                            }
                        >
                            {Object.entries(DOCUMENT_TYPES_LABELS).map(
                                ([value, label]) => (
                                    <MenuItem key={value} value={value}>
                                        {label}
                                    </MenuItem>
                                )
                            )}
                        </Select>
                    </FormControl>
                    <FormControl>
                        <TextField
                            value={idNumber || ""}
                            onChange={event => {
                                setIdNumber(event.target.value.toUpperCase());
                            }}
                            autoCapitalize="on"
                            id="id_number"
                            label="Número Documento"
                        />
                    </FormControl>
                </Grid>
                <Grid item>
                    <Button
                        // variant="contained"
                        color="secondary"
                        onClick={handleClearClick}
                    >
                        Clear
                    </Button>
                    <FormControl>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={(): void =>
                                onSearchClicked(idType, idNumber)
                            }
                        >
                            Search
                        </Button>
                    </FormControl>
                </Grid>
            </Grid>
        </form>
    );
}

export { SearchCase };
