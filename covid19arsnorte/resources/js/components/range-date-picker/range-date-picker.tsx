import React, { useState } from "react";
import { DatePicker } from "../date-picker";
import { FormGroup } from "@material-ui/core";

interface RangeDatePickerProps {
    startLabel: string;
    endLabel: string;
    startValue: Date;
    endValue: Date;
    disabled?: boolean;
    format?: string;
    required?: boolean;
    autoOk?: boolean;
    disableFuture?: boolean;
    onStartDateChange: (date: Date) => void;
    onEndDateChange: (date: Date) => void;
}

function RangeDatePicker({
    startLabel,
    endLabel,
    startValue,
    endValue,
    disabled,
    format,
    required,
    onStartDateChange,
    onEndDateChange,
    autoOk,
    disableFuture
}: RangeDatePickerProps): JSX.Element {
    const [minDate, setMinDate] = useState<Date>();
    const [maxDate, setMaxDate] = useState<Date>();

    return (
        <>
            <FormGroup>
                <DatePicker
                    label={startLabel}
                    value={startValue}
                    disabled={disabled}
                    format={format}
                    required={required}
                    maxDate={maxDate}
                    maxDateMessage="A data incial deve ser anterior à final."
                    autoOk={autoOk}
                    disableFuture={disableFuture}
                    onDateChange={(date: Date): void => {
                        setMinDate(date);
                        onStartDateChange(date);
                    }}
                ></DatePicker>
            </FormGroup>
            <FormGroup>
                <DatePicker
                    label={endLabel}
                    value={endValue}
                    disabled={disabled}
                    format={format}
                    required={required}
                    minDate={minDate}
                    minDateMessage="A data final deve ser posterior à inicial"
                    autoOk={autoOk}
                    disableFuture={disableFuture}
                    onDateChange={(date: Date): void => {
                        setMaxDate(date);
                        onEndDateChange(date);
                    }}
                ></DatePicker>
            </FormGroup>
        </>
    );
}

export { RangeDatePicker };
