import React, { useState } from "react";
import { Person } from "./types";
import {
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    TablePagination,
    TextField,
    MenuItem,
    IconButton
} from "@material-ui/core";
import { Done, Clear } from "@material-ui/icons";
import { DOCUMENT_TYPES_LABELS } from "../../constants";

interface PeopleTableProps {
    people: Person[];
    page: number;
    rowsPerPage: number;
    totalRows: number;
    onPersonChange: (person: Person) => void;
    onChangePage: (
        event: React.MouseEvent<HTMLButtonElement> | null,
        page: number
    ) => void;
    onChangeRowsPerPage?: React.ChangeEventHandler<
        HTMLTextAreaElement | HTMLInputElement
    >;
}

interface Column {
    id: string;
    label: string;
}

const columns: Column[] = [
    {
        id: "documentId",
        label: "ID"
    },
    {
        id: "documentType",
        label: "Tipo de Documento"
    },
    {
        id: "name",
        label: "Nome"
    },
    {
        id: "registration",
        label: "Matrícula"
    },
    {
        id: "arrived",
        label: "Chegada"
    },
    {
        id: "positive",
        label: "Resultado"
    },
    {
        id: "actions",
        label: ""
    }
];

interface PeopleRowProps {
    person: Person;
    onPersonChange: (person: Person) => void;
}

function PeopleRow({ person, onPersonChange }: PeopleRowProps): JSX.Element {
    const [unsavedPerson, setUnsavedPerson] = useState(person);

    const result = unsavedPerson.hasContractedDisease
        ? "YES"
        : unsavedPerson.hasContractedDisease === null
        ? "UNKNOWN"
        : "NO";
    const hasArrived = unsavedPerson.hasArrived
        ? "YES"
        : unsavedPerson.hasArrived === null
        ? "UNKNOWN"
        : "NO";

    const possibleChangeKeys: (keyof Person)[] = [
        "registration",
        "hasArrived",
        "hasContractedDisease"
    ];
    const hasChanges = possibleChangeKeys.some(
        key => unsavedPerson[key] !== person[key]
    );

    const handleArrivedChange = (person: Person, newStatus: string): void => {
        if (newStatus === "UNKNOWN") {
            setUnsavedPerson({ ...person, hasArrived: null });
            return;
        }
        setUnsavedPerson({ ...person, hasArrived: newStatus === "YES" });
    };

    const handleContractedDiseaseChange = (
        person: Person,
        newStatus: string
    ): void => {
        if (newStatus === "UNKNOWN") {
            setUnsavedPerson({ ...person, hasContractedDisease: null });
            return;
        }

        setUnsavedPerson({
            ...person,
            hasContractedDisease: newStatus === "YES"
        });
    };

    const handleRegistrationChange = (
        person: Person,
        newRegistration: string
    ): void => {
        setUnsavedPerson({
            ...person,
            registration: newRegistration
        });
    };

    const handleClear = (): void => {
        setUnsavedPerson(person);
    };
    const handleDone = (): void => {
        onPersonChange(unsavedPerson);
    };

    const renderActions = (): JSX.Element => (
        <div style={{ display: "flex", justifyContent: "space-around" }}>
            <IconButton
                color="secondary"
                size="small"
                aria-label="Cancela"
                onClick={handleClear}
            >
                <Clear />
            </IconButton>
            <IconButton
                color="primary"
                size="small"
                aria-label="Grava"
                onClick={handleDone}
            >
                <Done />
            </IconButton>
        </div>
    );

    return (
        <TableRow key={unsavedPerson.key} hover>
            <TableCell>{unsavedPerson.id}</TableCell>
            <TableCell>
                {DOCUMENT_TYPES_LABELS[unsavedPerson.documentType]}
            </TableCell>
            <TableCell>{unsavedPerson.name}</TableCell>
            <TableCell>
                <TextField
                    value={unsavedPerson.registration}
                    onChange={(event): void => {
                        handleRegistrationChange(
                            unsavedPerson,
                            event.target.value
                        );
                    }}
                ></TextField>
            </TableCell>
            <TableCell>
                <TextField
                    select
                    value={hasArrived}
                    style={{ minWidth: 101 }}
                    onChange={(event): void =>
                        handleArrivedChange(unsavedPerson, event.target.value)
                    }
                >
                    <MenuItem value="YES">Sim</MenuItem>
                    <MenuItem value="NO">Não</MenuItem>
                    <MenuItem value="UNKNOWN">Cancelado</MenuItem>
                </TextField>
            </TableCell>
            <TableCell>
                <TextField
                    select
                    value={result}
                    style={{ minWidth: 101 }}
                    onChange={(event): void =>
                        handleContractedDiseaseChange(
                            unsavedPerson,
                            event.target.value
                        )
                    }
                >
                    <MenuItem value="UNKNOWN">Desconhecido</MenuItem>
                    <MenuItem value="YES">Positivo</MenuItem>
                    <MenuItem value="NO">Negativo</MenuItem>
                </TextField>
            </TableCell>
            <TableCell style={{ width: 100 }}>
                {hasChanges && renderActions()}
            </TableCell>
        </TableRow>
    );
}

function PeopleTable({
    people,
    page,
    rowsPerPage,
    totalRows,
    onPersonChange,
    onChangePage,
    onChangeRowsPerPage
}: PeopleTableProps): JSX.Element {
    const renderColumn = (column: Column): JSX.Element => (
        <TableCell key={column.id}> {column.label} </TableCell>
    );
    const renderPeopleRow = (person: Person): JSX.Element => {
        return (
            <PeopleRow
                key={person.key}
                person={person}
                onPersonChange={onPersonChange}
            ></PeopleRow>
        );
    };

    return (
        <TableContainer>
            <Table stickyHeader aria-label="lista-pessoas">
                <TableHead>
                    <TableRow>
                        <TablePagination
                            colSpan={columns.length}
                            count={totalRows}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={onChangePage}
                            onChangeRowsPerPage={onChangeRowsPerPage}
                        />
                    </TableRow>
                    <TableRow>{columns.map(renderColumn)}</TableRow>
                </TableHead>
                <TableBody>{people.map(renderPeopleRow)}</TableBody>
            </Table>
        </TableContainer>
    );
}

export { PeopleTable };
