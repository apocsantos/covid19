import React, { useState, useEffect, useRef, useMemo } from "react";
import { usePeople, getPeople, search } from "./services";
import { Person, PeopleMap } from "./types";
import { PeopleTable } from "./people-table";
import { SearchCase } from "../search-case";
import { DOCUMENT_TYPES, GLOBAL_ACTION_TYPE } from "../../types";
import { TextField, CardContent, Card, Grid } from "@material-ui/core";
import { useDebounce } from "use-debounce";
import { useGlobalState } from "../../context";

function PeopleListContainer(): JSX.Element {
    const perceivedMap = useRef<PeopleMap>({});
    const [page, setPage] = useState(1);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [{ peopleMap }, dispatch] = useGlobalState();
    const [lastUpdated, updatePerson] = usePeople(page, rowsPerPage);
    const [isInLastPage, setIsInLastPage] = useState(false);
    const [peopleList, setPeopleList] = useState<Person[]>([]);
    const [registration, setRegistrationQuery] = useState("");
    const [registrationQuery] = useDebounce(registration, 250);
    const [idTuple, setSearchedId] = useState<{
        idType: DOCUMENT_TYPES;
        id: string;
    }>();
    const { idType = "", id = "" } = idTuple ?? {};
    const tablePage = page - 1;

    const potentialDisplayedPeopleList = useMemo(() => {
        return peopleList
            .filter((person: Person) => {
                if (!id) {
                    return true;
                }

                return (
                    person.documentType === idType &&
                    person.id
                        .toLocaleLowerCase()
                        .includes(id.toLocaleLowerCase())
                );
            })
            .filter((person: Person) => {
                if (!registrationQuery) {
                    return true;
                }

                return person.registration
                    .toLocaleLowerCase()
                    .includes(registrationQuery.toLocaleLowerCase());
            })
            .sort((entryA, entryB) => {
                return (
                    entryB.createdDate.getTime() - entryA.createdDate.getTime()
                );
            });
    }, [peopleList, id, idType, registrationQuery]);

    const displayedPeople = useMemo(() => {
        return potentialDisplayedPeopleList.slice(
            tablePage * rowsPerPage,
            tablePage * rowsPerPage + rowsPerPage
        );
    }, [potentialDisplayedPeopleList, tablePage, rowsPerPage]);

    const handlePersonChange = (person: Person): void => {
        const perceivedPeopleMap = {
            ...peopleMap,
            ...perceivedMap.current,
            [person.key]: person
        };
        perceivedMap.current = perceivedPeopleMap;

        setPeopleList(Object.values(perceivedPeopleMap));
        updatePerson(person);
    };

    useEffect(() => {
        if (!idTuple && !registrationQuery) {
            return;
        }

        (async (): Promise<void> => {
            dispatch({ type: GLOBAL_ACTION_TYPE.START_LOADING });
            try {
                const searchResults = await search({
                    idType,
                    idNumber: id,
                    registration: registrationQuery
                });

                const payload = Object.fromEntries(
                    searchResults.map(person => [person.key, person])
                );
                dispatch({
                    type: GLOBAL_ACTION_TYPE.UPDATE_PEOPLE_MAP,
                    payload
                });
            } catch (error) {
            } finally {
                dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
            }
        })();
    }, [idTuple, registrationQuery]);

    useEffect(() => {
        (async (): Promise<void> => {
            if (isInLastPage) {
                return;
            }
            dispatch({ type: GLOBAL_ACTION_TYPE.START_LOADING });
            const { peopleMap, hasReachedEnd } = await getPeople(
                page + 1,
                rowsPerPage
            );

            setIsInLastPage(hasReachedEnd);

            dispatch({
                type: GLOBAL_ACTION_TYPE.UPDATE_PEOPLE_MAP,
                payload: peopleMap
            });
            dispatch({ type: GLOBAL_ACTION_TYPE.STOP_LOADING });
        })();
    }, [page, rowsPerPage]);

    useEffect(() => {
        lastUpdated && delete perceivedMap.current[lastUpdated.key];

        const newPeopleList = Object.values({
            ...peopleMap,
            ...perceivedMap.current
        });

        setPeopleList(newPeopleList);
    }, [peopleMap, id, idType, registrationQuery]);

    const handleClear = (): void => {
        setSearchedId(undefined);
    };

    const handleSearchClick = (idType: DOCUMENT_TYPES, id: string): void => {
        setPage(1);
        setSearchedId({ idType, id });
    };

    const handleRegistrationQueryChange = (event): void => {
        setRegistrationQuery(event.target.value);
    };

    const handleChangeRowsPerPage = (event): void => {
        setRowsPerPage(event.target.value);
        setPage(1);
    };

    const handleChangePage = (event, newPage): void => {
        const serverPage = newPage + 1;

        if (serverPage < page) {
            setIsInLastPage(false);
        }

        setPage(serverPage);
    };

    return (
        <Grid container direction="row" spacing={3}>
            <Grid item xs={12}>
                <TextField
                    value={registration}
                    label="Matricula"
                    onChange={handleRegistrationQueryChange}
                ></TextField>
                <SearchCase
                    onClear={handleClear}
                    onSearchClicked={handleSearchClick}
                ></SearchCase>
            </Grid>
            <Grid item xs={12}>
                <Card>
                    <CardContent>
                        <PeopleTable
                            page={tablePage}
                            rowsPerPage={rowsPerPage}
                            people={displayedPeople}
                            totalRows={peopleList.length}
                            onPersonChange={handlePersonChange}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        ></PeopleTable>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
}

export { PeopleListContainer };
