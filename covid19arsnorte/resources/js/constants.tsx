import { DOCUMENT_TYPES } from "./types";

export const DOCUMENT_TYPES_LABELS = {
    [DOCUMENT_TYPES.PASSPORT]: "Passaporte",
    [DOCUMENT_TYPES.CITIZEN_CARD]: "Cartão Cidadão",
    [DOCUMENT_TYPES.SNS]: "Numero Utente/SNS",
    [DOCUMENT_TYPES.DRIVERS_LICENSE]: "Carta condução"
};
