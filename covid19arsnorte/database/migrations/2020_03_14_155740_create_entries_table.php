<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->string('id');
            $table->string('name')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('gender')->nullable();
            $table->string('id_number');
            $table->enum('id_type', ['SNS', 'PASSPORT', 'CITIZEN_CARD', 'DRIVERS_LICENSE']);
            $table->string('contact')->nullable();
            $table->string('address')->nullable();
            $table->string('registration')->nullable();
            $table->string('nationality')->nullable();
            $table->string('profession')->nullable();
            $table->string('email')->nullable();
            $table->date('date_first_symptoms')->nullable();
            $table->enum('fever', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('cough', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('chills', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('otophagia', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('coryza', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('headaches', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('myalgia', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('dyspnoea', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('abdominal_pain', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('vomits', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('diarrhea', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->string('other_sympthoms')->nullable();
            $table->string('outside_country_residence')->nullable();
            $table->string('location')->nullable();
            $table->string('friend_contact')->nullable();
            $table->string('recent_trips')->nullable();
            $table->date('trip_departure')->nullable();
            $table->date('trip_return')->nullable();
            $table->date('trip_return_portugal')->nullable();
            $table->enum('contact_confirmed_case', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->string('contact_confirmed_case_name')->nullable();
            $table->date('contact_confirmed_case_date')->nullable();
            $table->enum('is_health_profissional', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('registered_on', ['HEADQUARTERS', 'LAB'])->default('HEADQUARTERS');
            $table->enum('has_arrived', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->enum('is_positive', ['YES', 'NO', 'UNKNOWN'])->default('UNKNOWN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
