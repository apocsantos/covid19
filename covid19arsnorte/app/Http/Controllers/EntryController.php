<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\PositiveEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EntryController extends Controller
{
    private $headquartersClaim = 'HEADQUARTERS';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size = $request->size ?? 10;

        if(Auth::user()->isLabUser()) {
            return Entry::select('id', 'name', 'id_number','id_type',
                'registration','has_arrived', 'is_positive')
                ->where(['approved' => 'YES'])
                ->orderBy('created_at', 'desc')
                ->simplePaginate($size);
        }
        else {
            return Entry::orderBy('created_at', 'desc')->simplePaginate($size);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $registered_on = $request->user()->user_type;

        if ($registered_on == 'ADMIN') {
            $registered_on = 'HEADQUARTERS';
        }

        $request->request->add(['registered_on'=>$registered_on]);
        $requestData = $this->convertCaps( $request->all());

        $entry = Entry::create($requestData);

        return $entry->id;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Entry $entry
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($entryID)
    {
        $entry = Entry::where(['id' => $entryID]);

        if(Auth::user()->isLabUser()) {
            $entry = Entry::where(['approved' => 'YES'])->select('id', 'name', 'id_number','id_type',
                'registration','has_arrived', 'is_positive');
        }

        return $entry->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Entry $entry)
    {
        $is_positive_old = $entry->is_positive;
        $is_positive_new = $request->input('is_positive');

        if($is_positive_old != 'YES' && $is_positive_new == 'YES') {
            Mail::to(config('mail.reply_to.address'))->send(new PositiveEmail($entry));
        }

        if(Auth::User()->isLabUser())
        {
            $res = $entry->update([
                'IS_POSITIVE' => strtoupper($request->input('IS_POSITIVE')),
                'HAS_ARRIVED' => strtoupper($request->input('HAS_ARRIVED'))
            ]);

            return response()->json($res);
        }

        $requestData = $this->convertCaps($request->all());

        $updateSuccess = $entry->update($requestData);
        return response()->json($updateSuccess);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Entry $entry
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Entry $entry)
    {
        return response('Not Implemented', 501);
    }

    public function search(Request $request) {
        $documentType = $request->get('id_type');
        $documentId = $request->get('id_number');
        $registration = $request->get('registration');

        if((!$documentId || !$documentType) && (!$registration)) {
           return [
               'error' => 'You need to specify the id_number and the id_type params or the registration.'
           ];
        }

        $patient = null;
        $clauses = [];

        if ($documentId) {
            $clauses = [
                ['id_type', '=', strtoupper($documentType)],
                ['id_number', 'LIKE', '%'.strtoupper($documentId).'%']
            ];
        } else if ($registration) {
            $clauses = [
                ['registration', 'LIKE', '%'.strtoupper($registration).'%']
            ];
        }

        if(Auth::user()->isLabUser()) {
            array_push($clauses, ['approved', '=', 'YES']);
        }

        $patient = Entry::where($clauses);

        $patient = $patient->orderBy('created_at', 'desc');

        if(Auth::user()->isLabUser()) {
            return $patient->SELECT(['id', 'name', 'id_number','id_type', 'registration','has_arrived', 'is_positive'])->get();
        }

        return $patient->get();
    }

    private function convertCaps(array $data) {
        $requestData = collect($data);

        $keys = array('fever', 'cough', 'chills', 'otophagia', 'coryza', 'headaches', 'myalgia',
                      'dyspnoea', 'abdominal_pain', 'vomits', 'diarrhea', 'id_type', 'contact_confirmed_case',
                      'is_health_profissional', 'registered_on', 'has_arrived', 'is_positive');

        foreach ($keys as $key) {
            $value = $requestData->get($key);

            if (!is_null($value)) {
                $requestData->put($key,  strtoupper($value));
            }
        }

        return $requestData->toArray();
    }
}
