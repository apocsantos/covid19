<?php

namespace App\Http\Middleware;

use Closure;
use App\Providers\RouteServiceProvider;

class AdminCheckMvc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->user()->user_type == 'ADMIN')
        {
            return $next($request);
        }

        return redirect('/');
    }
}
